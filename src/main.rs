#[get("/")]
async fn index() -> impl Responder {
    "Welcome! append '/word' to the url to convert the word to phonemes."
}

use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use regex::Regex;
use std::io;

struct ASRPhonemeGenerator {
    word2phone: HashMap<String, Vec<String>>,
}

impl ASRPhonemeGenerator {
    fn new() -> io::Result<Self> {
        let mut generator = ASRPhonemeGenerator {
            word2phone: HashMap::new(),
        };

        let file = File::open("cmudict_SPHINX_40")?;
        let reader = BufReader::new(file);
        let re = Regex::new(r#"" "|\t"#).unwrap();

        for line in reader.lines() {
            let line = line?;
            let content: Vec<&str> = re.split(&line).collect();
            let word = content[0].to_lowercase();
            let phone = content[1..].join(" ");

            generator.word2phone.entry(word).or_insert_with(Vec::new).push(phone);
        }

        Ok(generator)
    }
}

#[get("/{word}")]
async fn convert_word_to_phonemes(word: web::Path<String>) -> impl Responder {
    let generator = ASRPhonemeGenerator::new().expect("Failed to create generator");

    let word = word.into_inner().to_lowercase();
    match generator.word2phone.get(&word) {
        Some(phones) => {
            // Assuming you want to return the first phoneme set as a string
            let phoneme_str = phones.get(0).unwrap_or(&String::new()).clone();
            HttpResponse::Ok().content_type("text/plain").body(phoneme_str)
        },
        None => HttpResponse::NotFound().body(format!("No phonemes found for word: {}", word))
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(index)
            .service(convert_word_to_phonemes)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}

