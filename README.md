# Rust Microservice: Phoneme Conversion
In this project, I create a rust microservice takes a word as input and returns its phoneme representation using the CMU Sphinx's phonetic dictionary. If the input word is found in the dictionary, the function returns its phonemes; otherwise, it indicates that the word is unknown. 

## Goals
* Simple REST API/web service in Rust
* Dockerfile to containerize service
* CI/CD pipeline files

## Demo
![Video Demo](./video.mp4)

## Steps

### Step 1: Initialize Rust Project
1. Create a template by
    ```
    cargo new <PROJECT_NAME>
    ```
2. Add needed dependencies to `Cargo.toml`
3. Write content in the `src/main.rs`
4. Verify the application locally by `cargo run`

### Step 2: Create a Dockerfile
Create a Dockerfile in the root of your project. This file will contain instructions for building the Docker image of your Actix web app.
```dockerfile
# Use the right version correponding to your rust version
FROM rust:latest AS builder

# set up work directory
WORKDIR /myapp
USER root

# copy the entire project into the working dicrectory
COPY . .

# compile rust app in the working directory
RUN cargo build --release

# use the right image according to different versions of glibc
FROM debian:bookworm-slim

# set up working directory
WORKDIR /myapp

# copy the executable file to the working directory for easily launching
COPY --from=builder /myapp/target/release/actix_web_app /myapp
COPY cmudict_SPHINX_40 /myapp

# expose port
EXPOSE 8080

# run the app
CMD ["./actix_web_app"]
```
### Step 3: Build the Docker image and run container
#### Build your Docker image by
```
docker build -t <YOUR_IMAGE> .
```

#### Run the container locally by
```
docker run -p 8080:8080 <YOUR_IMAGE>
```
Then you can check your web application if running.

To reuse an existing Docker container, use:
```
docker start <CONTAINER_ID>
```

### Step 4: CI/CD Pipeline
Enable the CI/CD pipeline by creating the .gitlab-ci.yml file.
```yaml
image: docker:25.0.3

variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

services:
  - docker:dind

stages:
  - test

test:
  stage: test
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build -t image .
    - docker run -d --name container -p 8080:8080 image
    - docker ps -a
```


